package main

type TableInfo struct {
	PackageName string
	FileName    string
	StructName  string
	ColumnInfos []*ColumnInfo
	Imports     []string
}

type ColumnInfo struct {
	ColName string // 字段名称
	Type    string // 类型
	JsonCol string // json字段
	DBCol   string // db字段
	GenJson bool   // 是否生成json字段
	GenDB   bool   // 是否生成db字段
}
