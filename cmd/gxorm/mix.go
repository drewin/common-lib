package main

import (
	"os"
	"strings"
)

var sqlToGoType = map[string]string{
	"TEXT":     "string",
	"TINYINT":  "int",
	"SMALLINT": "int",
	"INT":      "int",
	"BIGINT":   "int64",
	"FLOAT":    "float32",
	"DOUBLE":   "float64",
	"DECIMAL":  "float64",
	"VARCHAR":  "string",
	"CHAR":     "string",
	"BLOB":     "[]uint8",
	"BOOL":     "bool",
	"DATETIME": "time.Time",
}

func get(sqlType string) string {
	v, ok := sqlToGoType[sqlType]
	if !ok {
		return ""
	}

	return v
}

func genTableName(structPre, columnName string) string {
	columnNames := strings.Split(columnName, "_")
	structName := structPre

	for _, one := range columnNames {
		if one == "" {
			continue
		}

		structName += firstUpper(one)
	}

	return structName

}

func firstUpper(str string) string {
	if str == "" {
		return ""
	}
	return strings.ToUpper(str[:1]) + str[1:]

}

func genColumnStructName(columnName string) string {
	columnNames := strings.Split(columnName, "_")
	structName := ""

	for _, one := range columnNames {
		if isNeedToUpper(one) {
			structName += strings.ToUpper(one)
			continue
		}

		structName += firstUpper(one)
	}

	return structName
}

func genColumnJsonName(columnName string) string {
	columnNames := strings.Split(columnName, "_")
	jsonName := ""

	for index, one := range columnNames {
		if index == 0 {
			jsonName += one
			continue
		}

		if isNeedToUpper(one) {
			jsonName += strings.ToUpper(one)
			continue
		}

		jsonName += firstUpper(one)
	}

	return jsonName
}

func alreadyImports(packageName string, imports []string) bool {
	if len(imports) <= 0 {
		return false
	}

	for _, one := range imports {
		if packageName == one {
			return true
		}
	}

	return false
}

func isNeedToUpper(word string) bool {
	word = strings.ToLower(word)

	b, err := os.ReadFile("needToUpper.txt")
	if err != nil {
		panic(err)
	}

	txt := string(b)
	if txt == "" {
		return false
	}

	for _, one := range strings.Split(txt, ",") {
		if one == "" {
			continue
		}

		if one == word {
			return true
		}
	}

	return false
}
