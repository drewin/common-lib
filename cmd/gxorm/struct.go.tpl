package {{.PackageName}}

{{$iLen := len .Imports}}
{{if gt $iLen 0}}
import (
	{{range .Imports}}
	    "{{.}}"
	{{end}}
)
{{end}}

type {{.StructName}} struct {
{{range .ColumnInfos}}  {{.ColName}}    {{.Type}}   `{{if .GenDB}}db:"{{.DBCol}}"{{end}}{{if .GenJson}}{{if .GenDB}} {{end}}json:"{{.JsonCol}}"{{end}}`
{{end}}}
