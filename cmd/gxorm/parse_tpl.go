package main

import (
	"html/template"
	"os"
	"os/exec"
)

func parse(tableInfo *TableInfo) {
	tmpl, err := template.ParseFiles("./struct.go.tpl")
	if err != nil {
		panic(err.Error())
		return
	}

	_, err = os.Stat("./" + tableInfo.PackageName)
	if os.IsNotExist(err) {
		err = os.Mkdir(tableInfo.PackageName, os.ModePerm)
		if err != nil {
			panic(err.Error())
		}
	}

	fileName := "./" + tableInfo.PackageName + "/" + tableInfo.FileName + ".go"
	file, err := os.Create(fileName)
	if err != nil {
		panic(err.Error())
	}

	err = tmpl.Execute(file, tableInfo)
	if err != nil {
		panic(err.Error())
	}

	cmd := exec.Command("go", "fmt", fileName)
	cmd.Stdout = os.Stdout
	cmd.Run()

}
