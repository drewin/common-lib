package lru

import (
	"fmt"
	"testing"
)

// 接口型函数: 指的是用函数实现接口、这样在调用的时候就非常方便、我们称这种函数为接口型函数、这种方式只适用于只有一个函数的接口
// 参考地址： https://cloud.tencent.com/developer/article/1196581
type Handler interface {
	Do(k, v interface{})
}

type HandlerFunc func(k, v interface{})

func (f HandlerFunc) Do(k, v interface{}) {
	f(k, v)
}

func Each(m map[interface{}]interface{}, h Handler) {
	if len(m) <= 0 {
		return
	}

	for k, v := range m {
		h.Do(k, v)
	}
}

func EachFunc(m map[interface{}]interface{}, f func(k, v interface{})) {
	Each(m, HandlerFunc(f))
}

type welcome string

func (w welcome) Do(k, v interface{}) {
	fmt.Printf("%s,我叫%s,今年%d岁\n", w, k, v)
}

type welcome1 string

func (w welcome1) SelfInfo(k, v interface{}) {
	fmt.Printf("%s,我叫%s,今年%d岁\n", w, k, v)
}

func selfInfo(k, v interface{}) {
	fmt.Printf("大家好,我叫%s,今年%d岁\n", k, v)
}

func TestHandlerDo(t *testing.T) {
	persons := make(map[interface{}]interface{})
	persons["张三"] = 20
	persons["李四"] = 23
	persons["王五"] = 26

	var w welcome = "大家好"

	Each(persons, w)

	var w1 welcome1 = "大家好"
	Each(persons, HandlerFunc(w1.SelfInfo))

	EachFunc(persons, w1.SelfInfo)

	EachFunc(persons, selfInfo)
}
