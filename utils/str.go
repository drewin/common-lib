package utils

import "strconv"

func Str2Int64(i string, defaultValue int64) int64 {
	val, err := strconv.ParseInt(i, 10, 64)
	if err != nil {
		return defaultValue
	}

	return val
}

func Str2Int32(i string, defaultValue int) int32 {
	return int32(Str2Int64(i, int64(defaultValue)))
}

func Str2Int(i string, defaultValue int) int {
	return int(Str2Int64(i, int64(defaultValue)))
}
