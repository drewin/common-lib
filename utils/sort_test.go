package utils

import (
	"fmt"
	"sort"
	"testing"
)

func TestInt64_Sort(t *testing.T) {
	s := Int64{10, 19, 22, 33}
	sort.Sort(s)
	fmt.Println(s)
}
