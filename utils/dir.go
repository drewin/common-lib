package utils

import (
	"io/ioutil"
)

func GetDirsByPath(baseFolderPath string) []string {
	dirs, _ := ioutil.ReadDir(baseFolderPath)

	rs := make([]string, 0, 5)

	for _, dir := range dirs {
		if dir.IsDir() {
			rs = append(rs, dir.Name())
		}
	}

	return rs
}
