package utils

import "sort"

type Int64 []int64

func (p Int64) Len() int           { return len(p) }
func (p Int64) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p Int64) Less(i, j int) bool { return p[i] < p[j] }
func (p Int64) Sort()              { sort.Sort(p) }
