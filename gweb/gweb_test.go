package gweb

import (
	"fmt"
	"net/http"
	"testing"
	"time"
)

func TestNewEngine(t *testing.T) {
	r := Default()
	v1 := r.Group("/v1")
	{
		v1.GET("/", func(c *Context) {
			c.HTML(http.StatusOK, "<h1>Hello Gee</h1>")
		})

		v1.GET("/hello", func(c *Context) {
			// expect /hello?name=geektutu
			c.String(http.StatusOK, "hello %s, you're at %s\n", c.Query("name"), c.Path)
		})
	}

	v2 := r.Group("/v2")
	v2.Use(cost()) // v2 group middleware
	{
		v2.GET("/hello/:name", func(c *Context) {
			// expect /hello/geektutu
			i := 1
			j := 0
			fmt.Println(i / j)

			c.String(http.StatusOK, "hello %s, you're at %s\n", c.Param("name"), c.Path)
		})
	}

	r.GET("/", func(c *Context) {
		c.HTML(http.StatusOK, "<h1>Hello Gee</h1>")
	})

	r.GET("/hello", func(c *Context) {
		// expect /hello?name=geektutu
		c.String(http.StatusOK, "hello %s, you're at %s\n", c.Query("name"), c.Path)
	})

	r.GET("/hello/:name", func(c *Context) {
		// expect /hello/geektutu
		c.String(http.StatusOK, "hello %s, you're at %s\n", c.Param("name"), c.Path)
	})

	r.GET("/assets/*filepath", func(c *Context) {
		c.JSON(http.StatusOK, H{"filepath": c.Param("filepath")})
	})

	r.Run(":9999")
}

func TestInsert(t *testing.T) {
	pattern := "/a/b"
	parts := parsePattern(pattern)
	fmt.Println(parts)

	n := new(node)
	n.insert(pattern, parts, 0)

	rs := n.search(parts, 0)
	fmt.Println(rs)
}

func cost() HandlerFunc {
	return func(c *Context) {
		start := time.Now().Unix()
		c.Next()

		fmt.Println("cost:", time.Now().Unix()-start)

	}
}
