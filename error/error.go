package error

import (
	"runtime"
	"strconv"
	"strings"
)

type Error struct {
	Code        int
	Message     string
	OrigMessage string
	OrigFile    string
}

func (e *Error) GetCode() int {
	return e.Code
}

func (e *Error) GetMessage() string {
	return e.Message
}

func (e *Error) GetOrigMessage() string {
	return e.OrigMessage
}

func NewError(errCode int, message, origMessage string) *Error {
	err := new(Error)
	err.Code = errCode
	err.Message = message
	pc, file, line, _ := runtime.Caller(1)
	err.OrigMessage = file + " line " + strconv.FormatInt(int64(line), 10) + ": " + "func " + runtime.FuncForPC(pc).Name() + " : " + origMessage

	str := strings.Split(file, "/")
	if len(str) > 0 {
		err.OrigFile = str[len(str)-1]
	}
	return err
}
