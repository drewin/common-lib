package web

import (
	"fmt"
	"net/http"
	"testing"
)

func fooHandler(http.ResponseWriter, *http.Request) {
	fmt.Println("fooHandler.")
}
func TestHttp(t *testing.T) {
	http.Handle("/foo", http.HandlerFunc(fooHandler))

	http.HandleFunc("/foo/info", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Println("/foo/info...")
	})

	http.ListenAndServe(":8080", nil)
}
