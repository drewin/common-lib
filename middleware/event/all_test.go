package event

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEvent(t *testing.T) {
	assert.Panics(t, func() {
		On(" ", func(d *Data) error {
			return nil
		})
	})

	On("n1", func(d *Data) error {
		fmt.Printf("event:%s", d.GetName())
		return nil
	})

	On("n2", func(d *Data) error {
		return fmt.Errorf("an error")
	})

	assert.True(t, Has("n1"))
	assert.False(t, Has("n1-exists"))

	assert.NotEmpty(t, DefaultEM.GetEventHandlers("n1"))
	assert.Contains(t, DefaultEM.EventHandlers(), "n1")

	assert.Error(t, Fire("n2"))
	assert.Panics(t, func() {
		MustFire("n2")
	})
}
