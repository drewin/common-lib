package event

var DefaultEM = NewManager()

func On(name string, handler HandlerFunc) {
	DefaultEM.On(name, handler)
}

func Has(name string) bool {
	return DefaultEM.HashEvent(name)
}

func Fire(name string, args ...interface{}) error {
	return DefaultEM.Fire(name, args)
}

func MustFire(name string, args ...interface{}) {
	DefaultEM.MustFire(name, args)
}
