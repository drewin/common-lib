package event

type Data struct {
	aborted bool
	name    string
	Data    []interface{}
}

func (d *Data) GetName() string {
	return d.name
}

func (d *Data) SetAborted(aborted bool) {
	d.aborted = aborted
}

func (d *Data) IsAborted() bool {
	return d.aborted
}

func (d *Data) init(name string, data []interface{}) {
	d.name = name
	d.Data = data
}

func (d *Data) reset() {
	d.name = ""
	d.Data = make([]interface{}, 0)
	d.aborted = false
}
